package com.example.desarrollador.memoriaalpha;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

/**
 * Created by kevin on 12/09/16.
 */
public class CloseFragment extends newFragment {
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view=inflater.inflate(R.layout.closedialog, container, false);
        view.findViewById(R.id.si).setOnClickListener(null);
        view.findViewById(R.id.no).setOnClickListener(null);
        view.findViewById(R.id.si).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                    free = false;
                    actual = v.getId();
                } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                    actual=-1;
                    StaticData.CURRENTPOINTS=0;
                    StaticData.LEVEl=0;
                    StaticData.SUB_LEVEL=0;
                    getactivity().finish();
                }
                return false;
            }
        });
        view.findViewById(R.id.no).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                    free = false;
                    actual = v.getId();
                } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                    actual=-1;
                    onBackPressed();
                    unlock();
                }
                return false;
            }
        });
        this.view=view;
        return view;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        final int animatorId = (enter) ? R.anim.zoom_in : R.anim.zoom_out;
        final Animation anim = AnimationUtils.loadAnimation(getActivity(), animatorId);
        anim.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setBackgroundColor(Color.parseColor("#96c6e431"));
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        view.setBackgroundColor(Color.TRANSPARENT);
        return anim;
    }

    @Override
    protected void onBackPressed() {
        getactivity().removeFragment();
        getactivity().getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.zoom_in,R.anim.zoom_out).remove(this).commit();
        if(getactivity().pila.size()==1) {
            LevelFragment fg = getactivity().getLevelFragment();
            if(fg!=null)
                fg.run();
        }
    }
}
