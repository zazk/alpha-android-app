package com.example.desarrollador.memoriaalpha;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;


public class PauseFragment extends newFragment {
    View view;
    ImageView v1;
    TextView puntos,puesto;
    int[] im={R.drawable.completado0,R.drawable.completado1,R.drawable.completado2};
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(view!=null) {
            if(StaticData.PLACE>0)
                puesto.setText("Puesto en Ranking: "+StaticData.PLACE+"º");
            else
                puesto.setText("Puesto en Ranking: - ");
            puntos.setText("Puntaje: "+StaticData.POINTS);
            v1.setImageResource(im[StaticData.LEVEl]);
            return view;
        }
        View v = inflater.inflate(R.layout.pausar, container, false);
        v1=((ImageView)v.findViewById(R.id.nivel));
        Typeface tf = Typeface.createFromAsset(getActivity().getApplicationContext().getAssets(),"fonts/ChineseRocksRg-Regular.ttf");
        puntos=((TextView)v.findViewById(R.id.puntos));
        puesto=((TextView)v.findViewById(R.id.puesto));
        puntos.setTypeface(tf);
        puesto.setTypeface(tf);
        if(StaticData.PLACE>0)
            puesto.setText("Puesto en Ranking: "+StaticData.PLACE+"º");
        else
            puesto.setText("Puesto en Ranking: - ");
        puntos.setText("Puntaje: "+StaticData.POINTS);
        v1.setImageResource(im[StaticData.LEVEl]);
        v.findViewById(R.id.continuar).setOnClickListener(null);
        v.findViewById(R.id.salir).setOnClickListener(null);
        v.findViewById(R.id.continuar).setOnTouchListener(
                new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                            free = false;
                            actual = v.getId();
                        } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                            actual=-1;
                            onBackPressed();
                            unlock();
                        }
                        return false;
                    }
                });

        v.findViewById(R.id.salir).setOnTouchListener(
                new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                            free = false;
                            actual = v.getId();
                        } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                            actual=-1;
                            salir();
                            unlock();
                        }
                        return false;
                    }
                });
        view=v;
        return v;
    }
    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        final int animatorId = (enter) ? R.anim.zoom_in : R.anim.zoom_out;
        final Animation anim = AnimationUtils.loadAnimation(getActivity(), animatorId);
        anim.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setBackgroundColor(Color.parseColor("#96c6e431"));
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        view.setBackgroundColor(Color.TRANSPARENT);
        return anim;
    }
    @Override
    protected void onBackPressed() {
        LevelFragment fg=(LevelFragment)getactivity().getLevelFragment();
        getactivity().removeFragment();
        getactivity().getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.zoom_in,R.anim.zoom_out).remove(this).commit();
        fg.run();
    }
}
