package com.example.desarrollador.memoriaalpha;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

public class ThirdLevelFragment extends LevelFragment {
    protected int[] hidden=null;
    protected int random_color[]=null;
    protected int colors[]={R.drawable.amarillo3,R.drawable.anaranjado3,R.drawable.celeste3,R.drawable.rosado3,
            R.drawable.verde3,R.drawable.violeta3};
    int random_color_aux[] = getRandomPermutation(6);
    protected int gris=R.drawable.gris3;
    protected void setRunnable(final View view){
        hidden=getRandomPermutation(6);
        random_color=getRandomPermutation(6);
        r=new Runnable() {
            @Override
            public void run() {
                if(first){
                    first=false;
                    for(int i=0;i<6;i++){
                        paleta[i].setImageResource(gris);
                        paleta[i].setOnTouchListener(null);
                    }
                    view.findViewById(R.id.ayuda).setOnTouchListener(null);
                    points.setText(""+StaticData.CURRENTPOINTS);
                    times.setText(""+time);
                    h.postDelayed(this,500);
                }
                else
                switch(option/6) {
                    case 0:
                        switch (option % 2) {
                            case 0:
                                paleta[hidden[option/2]].setImageResource(colors[random_color[hidden[option/2]]]);
                                break;
                            case 1:
                                paleta[hidden[option/2]].setImageResource(gris);
                                break;
                        }
                        option++;
                        h.postDelayed(this, 500);
                        break;
                    case 1:
                        switch (option % 2) {
                            case 0:
                                for (int j = 0; j < 6; j++) {
                                    paleta[j].setImageResource(colors[random_color_aux[j]]);
                                }
                                for (int i = 0; i < 3; i++) {
                                    final int finalI = i;
                                    final int key = find(random_color_aux, random_color[hidden[i]]);
                                    paleta[key]
                                            .setOnTouchListener(
                                                    new View.OnTouchListener() {
                                                        @Override
                                                        public boolean onTouch(View v, MotionEvent event) {
                                                            if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                                                                free = false;
                                                                actual = v.getId();
                                                            } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                                                                actual=-1;
                                                                if (correct == finalI) {
                                                                    StaticData.CURRENTPOINTS += (3 - correct) * time;
                                                                    correct++;
                                                                    points.setText("" + StaticData.CURRENTPOINTS);
                                                                    paleta[key].setAlpha(0.5f);
                                                                    paleta[key].setOnTouchListener(
                                                                            new View.OnTouchListener() {
                                                                                @Override
                                                                                public boolean onTouch(View v, MotionEvent event) {
                                                                                    if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                                                                                        free = false;
                                                                                        actual = v.getId();
                                                                                    } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                                                                                        actual=-1;
                                                                                        lose();
                                                                                        unlock();
                                                                                    }
                                                                                    return false;
                                                                                }
                                                                            });
                                                                    if (correct == 3) {
                                                                        if (StaticData.SUB_LEVEL < 9) {
                                                                            if (getactivity().nextSubLevelFragment == null)
                                                                                getactivity().nextSubLevelFragment = new NextSubLevelFragment();
                                                                            getactivity()
                                                                                    .getSupportFragmentManager()
                                                                                    .beginTransaction()
                                                                                    .add(R.id.mainContainer, ((MainActivity) getActivity()).nextSubLevelFragment)
                                                                                    .commit();
                                                                        } else {
                                                                            if (getactivity().nextLevelFragment == null)
                                                                                getactivity().nextLevelFragment = new NextLevelFragment();
                                                                            getactivity()
                                                                                    .getSupportFragmentManager()
                                                                                    .beginTransaction()
                                                                                    .add(R.id.mainContainer, new NextLevelFragment())
                                                                                    .commit();
                                                                        }
                                                                    }
                                                                } else {
                                                                    lose();
                                                                }
                                                                unlock();
                                                            }
                                                            return false;
                                                        }
                                                    });
                                }
                                for (int i = 3; i < 6; i++) {
                                    final int key = find(random_color_aux, random_color[hidden[i]]);
                                    paleta[key].setOnTouchListener(
                                            new View.OnTouchListener() {
                                                @Override
                                                public boolean onTouch(View v, MotionEvent event) {
                                                    if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                                                        free = false;
                                                        actual = v.getId();
                                                    } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                                                        actual=-1;
                                                        lose();
                                                        unlock();
                                                    }
                                                    return false;
                                                }
                                            });
                                }

                                view.findViewById(R.id.ayuda).setOnTouchListener(
                                        new View.OnTouchListener() {
                                            @Override
                                            public boolean onTouch(View v, MotionEvent event) {
                                                if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                                                    free = false;
                                                    actual = v.getId();
                                                } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                                                    actual=-1;
                                                    ayuda();
                                                    view.findViewById(R.id.ayuda).setOnTouchListener(null);
                                                }
                                                return false;
                                            }
                                        });
                                option++;
                                h.postDelayed(this, 1000);
                                break;
                            case 1:
                                time--;
                                times.setText(""+time);
                                if(time>0) {
                                    if(correct!=3) {
                                        h.postDelayed(this, 1000);
                                    }
                                }
                                else lose();
                                break;
                        }
                        break;
                }
            }
        };
    }
    public int find(int array[],int key){
        for(int i=0;i<array.length;i++){
            if(array[i]==key)
                return i;
        }
        return -1;
    }
}