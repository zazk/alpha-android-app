package com.example.desarrollador.memoriaalpha;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

public class LoginFragment extends newFragment{
    private LoginButton facebook;
    private CallbackManager callbackManager;

    private ProfileTracker mProfileTracker = null;

    ImageView facebook_aux;

    int x=0;

    FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {

            if(Profile.getCurrentProfile() == null) {
                mProfileTracker = new ProfileTracker() {
                    @Override
                    protected void onCurrentProfileChanged(Profile profile, Profile profile2) {
                        // profile2 is the new profile
                        mProfileTracker.stopTracking();
                        Profile.setCurrentProfile(profile2);
                        interfaz(profile2,true);
                    }
                };
                mProfileTracker.startTracking();
            }
            else {
                interfaz(Profile.getCurrentProfile(),true);
            }
        }
        @Override
        public void onCancel() {
            Toast.makeText(getActivity(),"Nos Conectamos con Facebook",Toast.LENGTH_SHORT).show();
        }
        @Override
        public void onError(FacebookException error) {
            Toast.makeText(getActivity(),"Hubo un error",Toast.LENGTH_SHORT).show();
        }
    };
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callbackManager = CallbackManager.Factory.create();
    }
    protected void interfaz(final Profile profile,final boolean firstlogged){
        if (profile != null){
            facebook_aux.setEnabled(false);
            getactivity().removeFragment();

            new AsyncTask<Profile, Void, String>() {
                ProgressDialog progress;
                @Override
                protected void onPreExecute() {
                    progress=new ProgressDialog(getActivity());
                    progress.setMessage("Obteniendo Datos");
                    progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progress.setIndeterminate(true);
                    progress.setProgress(0);
                    progress.show();
                }

                @Override
                protected String doInBackground(Profile... params) {
                    HashMap<String, String> parms = new HashMap();
                    parms.put("fbId",params[0].getId());
                    parms.put("nick",params[0].getName());
                    Log.i("mensaje",params[0].getId());
                    String output=JSONParser.makeHttpRequest("http://198.211.112.25:1234/getUser",parms);
                    return output;
                }
                @Override
                protected void onPostExecute(String s) {
                    //progress.dismiss();
                    if(s!=null){
                        try {
                            JSONObject jsonObject=new JSONObject(s);
                            try {
                                JSONObject object = jsonObject.getJSONObject("data");
                                StaticData.NAME = object.getString("nombre").split(" ")[0];
                                StaticData.POINTS = object.getInt("puntaje");
                                StaticData.PLACE = object.getInt("puesto");
                                StaticData.LEVEl = object.getInt("nivel");
                                StaticData.SUB_LEVEL = object.getInt("fase");
                                StaticData.stickers_ganados = object.getString("stickers");
                                if (jsonObject.getInt("response") == 1) {
                                    StaticData.hasProfile = true;
                                } else {
                                    StaticData.hasProfile = false;
                                }
                            }catch (Exception e){
                                StaticData.NAME = Profile.getCurrentProfile().getName().split(" ")[0];
                                StaticData.POINTS = 0;
                                StaticData.PLACE = -1;
                                StaticData.LEVEl = 0;
                                StaticData.SUB_LEVEL = 0;
                                StaticData.hasProfile = false;
                            }
                            progress.dismiss();
                            getactivity().getSupportFragmentManager()
                                    .beginTransaction()
                                    .replace(R.id.mainContainer, new StartLevel())
                                    .commit();
                        } catch (JSONException e) {
                            progress.dismiss();
                            e.printStackTrace();
                            facebook_aux.setEnabled(true);
                        }
                    }
                    else{
                        progress.dismiss();
                        facebook_aux.setEnabled(true);
                        Toast.makeText(getactivity(),"Hubo un error",Toast.LENGTH_SHORT).show();
                    }
                }
            }.execute(profile);
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        return inflater.inflate(R.layout.login, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){

        super.onViewCreated(view, savedInstanceState);
        getactivity().putFragment(this);
        facebook = (LoginButton) view.findViewById(R.id.facebook);
        facebook.setReadPermissions("user_friends");

        facebook.setFragment(this);
        facebook.registerCallback(callbackManager, callback);
        facebook_aux=(ImageView)view.findViewById(R.id.facebook_aux);
        facebook_aux.setOnClickListener(null);
        facebook_aux.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (free&&event.getAction()==MotionEvent.ACTION_DOWN) {
                    free = false;
                    actual=v.getId();
                    return true;
                }
                else if(event.getAction()==MotionEvent.ACTION_UP&&actual==v.getId()) {
                    actual=-1;
                    ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                    if (conMgr!= null && conMgr.getActiveNetworkInfo() != null) {
                        if(isLoggedIn())
                            interfaz(Profile.getCurrentProfile(),false);
                        else
                            facebook.performClick();
                            //Toast.makeText(getActivity(),"hay Conexion "+x++,Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getActivity(),"No hay Conexion",Toast.LENGTH_SHORT).show();
                    }
                    unlock();
                    return true;
                }
                return false;
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (callbackManager.onActivityResult(requestCode, resultCode, data)) {
            return;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public boolean isLoggedIn(){
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    @Override
    public void onResume() {
        super.onResume();
        ConnectivityManager conMgr = (ConnectivityManager) getactivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        if(isLoggedIn()){
            if (conMgr!=null && conMgr.getActiveNetworkInfo() != null) {
                interfaz(Profile.getCurrentProfile(),false);
            }
            else{
                Toast.makeText(getactivity(),"No hay Conexion",Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onBackPressed() {
        salir();
    }
}
