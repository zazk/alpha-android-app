package com.example.desarrollador.memoriaalpha;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

public class SecondLevelFragment extends LevelFragment {
    protected int colors[]={R.drawable.amarillo2,R.drawable.anaranjado2,R.drawable.celeste2,R.drawable.rosado2,
            R.drawable.verde2,R.drawable.violeta2};
    protected int[] hidden=null;
    protected int random_color[]=null;
    protected int gris=R.drawable.gris2;
    protected void setRunnable(final View view){
        hidden=getRandomPermutation(6);
        random_color=getRandomPermutation(6);
        r=new Runnable() {
            @Override
            public void run() {
                if(first){
                    first=false;
                    for(int i=0;i<6;i++){
                        paleta[i].setImageResource(gris);
                        paleta[i].setOnTouchListener(null);
                    }
                    view.findViewById(R.id.ayuda).setOnTouchListener(null);
                    points.setText(""+StaticData.CURRENTPOINTS);
                    times.setText(""+time);
                    h.postDelayed(this,500);
                }
                else
                    switch(option/6){
                        case 0:
                            switch(option%2){
                                case 0:
                                    paleta[hidden[option/2]].setImageResource(colors[random_color[hidden[option/2]]]);
                                    break;
                                case 1:
                                    paleta[hidden[option/2]].setImageResource(gris);
                                    break;
                            }
                            option++;
                            h.postDelayed(this,500);
                            break;
                        case 1:
                            switch (option%2) {
                                case 0:
                                    for (int i = 0; i < 3; i++) {
                                        final int finalI = i;
                                        paleta[hidden[i]]
                                                .setOnTouchListener(
                                                        new View.OnTouchListener() {
                                                            @Override
                                                            public boolean onTouch(View v, MotionEvent event) {
                                                                if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                                                                    free = false;
                                                                    actual = v.getId();
                                                                } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                                                                    actual=-1;
                                                                    if (correct == finalI) {
                                                                        StaticData.CURRENTPOINTS += (3 - correct) * time;
                                                                        correct++;
                                                                        points.setText("" + StaticData.CURRENTPOINTS);
                                                                        paleta[hidden[finalI]].setAlpha(0.5f);
                                                                        paleta[hidden[finalI]].setOnTouchListener(
                                                                                new View.OnTouchListener() {
                                                                                    @Override
                                                                                    public boolean onTouch(View v, MotionEvent event) {
                                                                                        if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                                                                                            free = false;
                                                                                            actual = v.getId();
                                                                                        } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                                                                                            actual=-1;
                                                                                            lose();
                                                                                            unlock();
                                                                                        }
                                                                                        return false;
                                                                                    }
                                                                                });
                                                                        if (correct == 3) {
                                                                            if (StaticData.SUB_LEVEL < 9) {
                                                                                if (getactivity().nextSubLevelFragment == null)
                                                                                    getactivity().nextSubLevelFragment = new NextSubLevelFragment();
                                                                                getactivity()
                                                                                        .getSupportFragmentManager()
                                                                                        .beginTransaction()
                                                                                        .add(R.id.mainContainer, ((MainActivity) getActivity()).nextSubLevelFragment)
                                                                                        .commit();
                                                                            } else {
                                                                                if (getactivity().nextLevelFragment == null)
                                                                                    getactivity().nextLevelFragment = new NextLevelFragment();
                                                                                getactivity()
                                                                                        .getSupportFragmentManager()
                                                                                        .beginTransaction()
                                                                                        .add(R.id.mainContainer,getactivity().nextLevelFragment)
                                                                                        .commit();
                                                                            }

                                                                        }
                                                                    } else {
                                                                        lose();
                                                                    }
                                                                    unlock();
                                                                }
                                                                return false;
                                                            }
                                                        });
                                    }
                                    for (int i = 3; i < 6; i++)
                                        paleta[hidden[i]].setOnTouchListener(
                                                new View.OnTouchListener() {
                                                    @Override
                                                    public boolean onTouch(View v, MotionEvent event) {
                                                        if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                                                            free = false;
                                                            actual = v.getId();
                                                        } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                                                            actual=-1;
                                                            lose();
                                                            unlock();
                                                        }
                                                        return false;
                                                    }
                                                });
                                    for (int i = 0; i < 6; i++)
                                        paleta[i].setImageResource(colors[random_color[i]]);
                                    view.findViewById(R.id.ayuda).setOnTouchListener(
                                            new View.OnTouchListener() {
                                                @Override
                                                public boolean onTouch(View v, MotionEvent event) {
                                                    if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                                                        free = false;
                                                        actual = v.getId();
                                                    } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                                                        actual=-1;
                                                        ayuda();
                                                        view.findViewById(R.id.ayuda).setOnTouchListener(null);
                                                    }
                                                    return false;
                                                }
                                            });
                                    option ++;
                                    h.postDelayed(this, 1000);
                                    break;
                                case 1:
                                    time--;
                                    times.setText(""+time);
                                    if(time>0) {
                                        if(correct!=3) {
                                            h.postDelayed(this, 1000);
                                        }
                                    }
                                    else lose();
                                    break;
                            }
                            break;
                    }
            }
        };
    }
}
