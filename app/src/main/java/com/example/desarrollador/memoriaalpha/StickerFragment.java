package com.example.desarrollador.memoriaalpha;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

public class StickerFragment extends newFragment {
    final private int code_request=1234;
    int stickers[];
    CallbackManager callbackManager;
    int actual=0;
    ViewPager viewPager;
    View view;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (callbackManager.onActivityResult(requestCode, resultCode, data)) {
            return;
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        callbackManager = CallbackManager.Factory.create();
        View v = inflater.inflate(R.layout.sticker_fragment, container, false);
        if(getArguments()==null) stickers=StaticData.getStickers();
        else {
            stickers = getArguments().getIntArray("stickers");
        }
        viewPager = (ViewPager) v.findViewById(R.id.viewpager);
        Typeface tf = Typeface.createFromAsset(getActivity().getApplicationContext().getAssets(),"fonts/ChineseRocksRg-Regular.ttf");
        ((TextView)v.findViewById(R.id.texto)).setTypeface(tf);
        ((TextView)v.findViewById(R.id.text)).setTypeface(tf);
        viewPager.setAdapter(new MyFragmentPagerAdapter(getChildFragmentManager(),stickers));
        viewPager.setOffscreenPageLimit(2); // the number of "off screen" pages to keep loaded each side of the current page
        v.findViewById(R.id.comparte).setOnClickListener(null);
        v.findViewById(R.id.salir).setOnClickListener(null);
        v.findViewById(R.id.premio).setOnClickListener(null);
        v.findViewById(R.id.profile).setOnClickListener(null);
        v.findViewById(R.id.descarga).setOnClickListener(null);
        v.findViewById(R.id.continuar).setOnClickListener(null);
        v.findViewById(R.id.continuar).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                    free = false;
                    actual = v.getId();
                } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                    actual=-1;
                    onBackPressed();
                    unlock();
                }
                return false;
            }
        });
        v.findViewById(R.id.comparte).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                    Log.i("mensaje","click down "+ SystemClock.elapsedRealtime());
                    free = false;
                    actual = v.getId();
                } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                    actual=-1;
                    ShareDialog shareDialog=new ShareDialog(StickerFragment.this);
                    shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                        @Override
                        public void onSuccess(Sharer.Result result) {
                            new AsyncTask<Profile, Void, String>() {
                                @Override

                                protected String doInBackground(Profile... params) {
                                    HashMap<String, String> parms = new HashMap();
                                    parms.put("fbId",params[0].getId());
                                    parms.put("score",""+100);
                                    parms.put("type","1");
                                    String output=JSONParser.makeHttpRequest("http://198.211.112.25:1234/addPoints",parms);
                                    return output;
                                }
                                @Override
                                protected void onPostExecute(String s) {
                                    try {
                                        JSONObject jsonObject=new JSONObject(s);
                                        StaticData.PLACE=jsonObject.getInt("rank");
                                        StaticData.POINTS+=100;
                                        Toast.makeText(getActivity(),"100 puntos mas",Toast.LENGTH_SHORT).show();
                                    } catch (JSONException e) {
                                        Toast.makeText(getActivity(),"Conectate a Internet",Toast.LENGTH_SHORT).show();
                                        e.printStackTrace();
                                    }
                                    unlock();
                                }
                            }.execute(Profile.getCurrentProfile());
                        }
                        @Override
                        public void onCancel() {
                            Log.i("mensaje","cancel");unlock();
                        }
                        @Override
                        public void onError(FacebookException error) {
                            unlock();
                        }
                    });
                    boolean facebookAppFound =   appInstalledOrNot("com.facebook.katana");
                    if(facebookAppFound) {
                        Bitmap image= BitmapFactory.decodeResource(getContext().getResources(),
                                StaticData.stickers[stickers[viewPager.getCurrentItem()]]);
                        SharePhoto photo = new SharePhoto.Builder()
                                .setBitmap(image)
                                .build();
                        SharePhotoContent content = new SharePhotoContent.Builder()
                                .addPhoto(photo)
                                .setShareHashtag(new ShareHashtag.Builder()
                                        .setHashtag("#MemoriaAlpha")
                                        .build())
                                .build();
                        shareDialog.show(content);
                    }
                    else {
                        ShareLinkContent content = new ShareLinkContent.Builder()
                                .setContentUrl(Uri.parse("http://forceclose.pe/"))
                                .setShareHashtag(new ShareHashtag.Builder()
                                        .setHashtag("#MemoriaAlpha")
                                        .build())
                                .setQuote("He conseguido ciertos puntos")
                                .build();
                        shareDialog.show(content);
                    }
                }
                return false;
            }
        });
        v.findViewById(R.id.salir).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                    free = false;
                    actual = v.getId();
                } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                    actual=-1;
                    salir();
                    unlock();
                }
                return false;
            }
        });
        v.findViewById(R.id.descarga).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                    free = false;
                    actual = v.getId();
                } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                    actual=-1;
                    actual=viewPager.getCurrentItem();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE
                        }, code_request);
                    }
                    else {
                        insertar();
                    }
                }
                return false;
            }
        });
        Picasso.with(getActivity())
                .load(Profile.getCurrentProfile().getProfilePictureUri(100, 100).toString())
                .into((ImageView)v.findViewById(R.id.profile));
        view=v;
        return v;
    }
    @Override
    public void onBackPressed() {
        if(game) {
            //si estoy en el juego
            for(int i=0;i<2;i++) {
                getactivity().removeFragment();
            }
            getactivity().getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.back_enter,R.anim.back_exit)
                    .replace(R.id.mainContainer,getactivity().getCurrentFragment())
                    .commit();
        }
        else {
            //si vengo de nextlevel
            if(hasopened)
                getactivity().removeFragment();
            getactivity().removeFragment();
            getactivity().nextLevelFragment.continuar();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(!game&&!hasopened){
            view.findViewById(R.id.profile).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                        free = false;
                        actual = v.getId();
                    } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()){
                        actual=-1;
                        hasopened=true;
                        newFragment fg=null;
                        if(StaticData.hasProfile) {
                            fg=new ProfileFragment();
                        }
                        else {
                            fg = new RegFragment();
                        }
                        getactivity().removeFragment();
                        getactivity().putFragment(fg);
                        getactivity().getSupportFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(R.anim.up_enter,R.anim.up_exit)
                                .replace(R.id.mainContainer, getactivity().getCurrentFragment())
                                .commit();
                        unlock();
                    }
                    return false;
                }
            });
            view.findViewById(R.id.premio).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                        free = false;
                        actual = v.getId();
                    } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                        actual=-1;
                        if(getactivity().premioFragment==null)
                            getactivity().premioFragment=new PremioFragment();
                        getactivity().removeFragment();
                        getactivity().putFragment(getactivity().premioFragment);
                        getactivity().getSupportFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(R.anim.enter,R.anim.exit)
                                .replace(R.id.mainContainer,getactivity().getCurrentFragment())
                                .commit();
                        unlock();
                    }
                    return false;
                }
            });

        }
        else {
            view.findViewById(R.id.premio).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    Log.i("mensaje",""+(actual==R.id.premio)+" "+game);
                    if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                        free = false;
                        actual = v.getId();
                    } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                        actual=-1;
                        if(getactivity().premioFragment==null)
                            getactivity().premioFragment=new PremioFragment();
                        getactivity().putFragment(getactivity().premioFragment);
                        getactivity().getSupportFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(R.anim.enter,R.anim.exit)
                                .replace(R.id.mainContainer,getactivity().getCurrentFragment())
                                .commit();
                        unlock();
                    }
                    return false;
                }
            });
            view.findViewById(R.id.profile).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                        free = false;
                        actual = v.getId();
                    } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                        actual=-1;
                        getactivity().removeFragment();
                        getactivity().getSupportFragmentManager().beginTransaction()
                                .setCustomAnimations(R.anim.back_enter,R.anim.back_exit)
                                .replace(R.id.mainContainer,getactivity().getCurrentFragment())
                                .commit();
                        unlock();
                    }
                    return false;
                }
            });
            view.findViewById(R.id.profile).setEnabled(false);
            ////////////////////////////////
            this.view=view;
            new MyHandler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    StickerFragment.this.view.findViewById(R.id.profile).setEnabled(true);
                }
            },700);
        }
    }

    protected void insertar(){
        File path = Environment.getExternalStorageDirectory();
        if (path.canWrite()) {
            Log.i("mensaje", "voy a guardar");
            File dir = new File(path.getAbsolutePath() + "/Download/");
            if (!dir.exists())
                dir.mkdirs();
            Bitmap icon = BitmapFactory.decodeResource(getactivity().getResources(),
                    StaticData.stickers[stickers[actual]]);
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(new File(dir, "Wallpaper"
                        + stickers[actual] + ".png"));
                icon.compress(Bitmap.CompressFormat.PNG, 100, out);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                        Toast.makeText(getContext(), "Descarga exitosa", Toast.LENGTH_SHORT)
                                .show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else
            Toast.makeText(getContext(), "Vuelva a intentarlo", Toast.LENGTH_SHORT)
                    .show();
        unlock();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case code_request:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    insertar();
                } else {
                    Toast.makeText(getContext(), "Permiso denegado", Toast.LENGTH_SHORT)
                            .show();
                    unlock();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
    private class MyFragmentPagerAdapter extends FragmentStatePagerAdapter {
        final int PAGE_COUNT;
        int imagenes[]=null;
        public MyFragmentPagerAdapter(FragmentManager fm,int imagenes[]){
            super(fm);
            this.imagenes = imagenes;
            PAGE_COUNT = imagenes.length;
        }
        @Override
        public Fragment getItem(int position) {
            Fragment f = new ImageFragment();
            Bundle arg = new Bundle();
            arg.putInt("dia",imagenes[position]);
            f.setArguments(arg);
            return f;
        }
        @Override
        public Parcelable saveState() {
            return null;
        }
        @Override
        public int getCount() {
            return PAGE_COUNT;
        }
    }
}