package com.example.desarrollador.memoriaalpha;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

public class Inicio extends AppCompatActivity {

    private ImageView  salir;
    private ImageView inicio;
    boolean free=true;
    int actual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inicio);

        inicio = (ImageView) findViewById(R.id.comenzar);
        salir = (ImageView) findViewById(R.id.salir);

        inicio.setOnClickListener(null);

        salir.setOnClickListener(null);

        inicio.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                    free = false;
                    actual = v.getId();
                } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                    actual=-1;
                    comenzar();
                    unlock();
                }
                return false;
            }
        });
        salir.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                    free = false;
                    actual = v.getId();
                } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                    actual=-1;
                    getSupportFragmentManager().beginTransaction()
                            //.setCustomAnimations(R.anim.zoom_in,R.anim.zoom_out)
                            .add(R.id.mainContainer,new CloseFragment2()).addToBackStack(null).commit();
                    unlock();
                }
                return false;
            }
        });


    }

    @Override
    public void onBackPressed() {
        Log.i("mensaje",getSupportFragmentManager().getBackStackEntryCount()+"");
        if(getSupportFragmentManager().getBackStackEntryCount()==0){
            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.zoom_in,R.anim.zoom_out)
                    .add(R.id.mainContainer,new CloseFragment2()).addToBackStack(null).commit();
        }
        else
        super.onBackPressed();
    }

    public  void comenzar(){
        Intent intent= new Intent(Inicio.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
    protected void unlock(){
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                free=true;
            }
        },50);
    }
}
