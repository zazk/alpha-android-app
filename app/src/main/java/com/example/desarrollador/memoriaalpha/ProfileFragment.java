package com.example.desarrollador.memoriaalpha;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.Profile;
import com.squareup.picasso.Picasso;

public class ProfileFragment extends newFragment {
    TextView puntos,puesto,nivel;
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        if(view !=null) {
            view.findViewById(R.id.continuar).setEnabled(false);
            new MyHandler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    view.findViewById(R.id.continuar).setEnabled(true);
                }
            },700);
            nivel.setText("Nivel "+StaticData.LEVEl+" Completados");
            if(StaticData.PLACE>0)
                puesto.setText("Puesto en Ranking: "+StaticData.PLACE+"º");
            else
                puesto.setText("Puesto en Ranking: - ");
            puntos.setText("Puntaje: "+StaticData.POINTS);
            return view;
        }
        View view=inflater.inflate(R.layout.profile_fragment, container, false);
        Profile profile= Profile.getCurrentProfile();
        Typeface tf = Typeface.createFromAsset(getActivity().getApplicationContext().getAssets(),"fonts/ChineseRocksRg-Regular.ttf");
        puntos=((TextView)view.findViewById(R.id.puntos));
        puesto=((TextView)view.findViewById(R.id.puesto));
        nivel=((TextView)view.findViewById(R.id.nivel));
        puntos.setTypeface(tf);
        puesto.setTypeface(tf);
        nivel.setTypeface(tf);
        nivel.setText("Nivel "+StaticData.LEVEl+" Completados");
        puesto.setText("Puesto en Ranking: "+StaticData.PLACE+"º");
        puntos.setText("Puntaje: "+StaticData.POINTS);
        ((TextView)view.findViewById(R.id.name)).setTypeface(tf);
        ((TextView)view.findViewById(R.id.name)).setText(StaticData.NAME);
        Picasso.with(getActivity())
                .load(profile.getProfilePictureUri(100, 100).toString())
                .into((ImageView)view.findViewById(R.id.profile));
        ///////////////////////
        view.findViewById(R.id.continuar).setOnClickListener(null);
        view.findViewById(R.id.ranking).setOnClickListener(null);
        view.findViewById(R.id.sticker).setOnClickListener(null);
        view.findViewById(R.id.salir).setOnClickListener(null);
        view.findViewById(R.id.continuar).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (free&&event.getAction()==MotionEvent.ACTION_DOWN) {
                    free=false;
                    actual=v.getId();
                }
                else if(event.getAction()==MotionEvent.ACTION_UP&&actual==v.getId()){
                    actual=-1;
                    onBackPressed();
                    unlock();
                }
                return false;
            }
        });
        view.findViewById(R.id.ranking).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (free&&event.getAction()==MotionEvent.ACTION_DOWN) {
                    free=false;
                    actual=v.getId();
                }
                else if(event.getAction()==MotionEvent.ACTION_UP&&actual==v.getId()){
                    actual=-1;
                    if(getactivity().rankingFragment==null)
                        getactivity().rankingFragment=new RankingFragment();
                    getactivity().putFragment(getactivity().rankingFragment);
                    getactivity().getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.enter,R.anim.exit)
                            .replace(R.id.mainContainer,getactivity().getCurrentFragment())
                            .commit();
                    unlock();
                }
                return false;
            }
        });
        view.findViewById(R.id.sticker).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (free&&event.getAction()==MotionEvent.ACTION_DOWN) {
                    free=false;
                    actual=v.getId();
                }
                else if(event.getAction()==MotionEvent.ACTION_UP&&actual==v.getId()){
                    actual=-1;
                    Log.i("mensaje","di click");
                    getactivity().putFragment(new StickerFragment());
                    getactivity().getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.enter,R.anim.exit)
                            .replace(R.id.mainContainer,getactivity().getCurrentFragment())
                            .commit();
                    unlock();
                }
                return false;
            }
        });
        view.findViewById(R.id.salir).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (free&&event.getAction()==MotionEvent.ACTION_DOWN) {
                    free=false;
                    actual=v.getId();
                }
                else if(event.getAction()==MotionEvent.ACTION_UP&&actual==v.getId()){
                    actual=-1;
                    salir();
                    unlock();
                }
                return false;
            }
        });
        view.findViewById(R.id.continuar).setEnabled(false);
        ////////////////////////////////
        this.view=view;
        new MyHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ProfileFragment.this.view.findViewById(R.id.continuar).setEnabled(true);
            }
        },700);
        return view;
    }

    @Override
    protected void onBackPressed() {
        if(game) {
            getactivity().removeFragment();
            getactivity().getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.back_enter,R.anim.back_exit)
                    .replace(R.id.mainContainer,getactivity().getCurrentFragment())
                    .commit();
        }
        else{
            getactivity().removeFragment();
            getactivity().nextLevelFragment.continuar();
        }
    }
}