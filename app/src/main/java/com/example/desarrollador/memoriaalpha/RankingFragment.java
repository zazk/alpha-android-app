package com.example.desarrollador.memoriaalpha;

import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Profile;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class RankingFragment extends newFragment {
    int[] name={R.id.name1,R.id.name2,R.id.name3,R.id.name4,R.id.name5};
    int[] point={R.id.points1,R.id.points2,R.id.points3,R.id.points4,R.id.points5};
    int[] photo={R.id.photo1,R.id.photo2,R.id.photo3,R.id.photo4,R.id.photo5};
    TextView names[]=new TextView[5];
    TextView points[]=new TextView[5];
    ImageView photos[]=new ImageView[5];
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(view!=null){
            return view;
        }
        View view = inflater.inflate(R.layout.ranking_fragment, container, false);
        Typeface tf = Typeface.createFromAsset(getActivity().getApplicationContext().getAssets(),"fonts/ChineseRocksRg-Regular.ttf");

        for(int i=0;i<5;i++){
            names[i]=(TextView)view.findViewById(name[i]);
            points[i]=(TextView)view.findViewById(point[i]);
            photos[i]=(ImageView)view.findViewById(photo[i]);
            names[i].setTypeface(tf);
            points[i].setTypeface(tf);
        }

        view.findViewById(R.id.continuar).setOnClickListener(null);
        view.findViewById(R.id.salir).setOnClickListener(null);

        view.findViewById(R.id.salir).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (free&&event.getAction()==MotionEvent.ACTION_DOWN) {
                    free = false;
                    actual=v.getId();
                }
                else if(event.getAction()==MotionEvent.ACTION_UP&&actual==v.getId()) {
                    actual=-1;
                    salir();unlock();
                }
                return false;
            }
        });
        view.findViewById(R.id.continuar).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (free&&event.getAction()==MotionEvent.ACTION_DOWN) {
                    free = false;
                    actual=v.getId();
                }
                else if(event.getAction()==MotionEvent.ACTION_UP&&actual==v.getId()) {
                    actual=-1;
                    onBackPressed();unlock();
                }
                return false;
            }
        });
        this.view=view;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new AsyncTask<Profile, Void, String>() {
            @Override
            protected String doInBackground(Profile... params) {
                HashMap<String, String> parms = new HashMap();
                String output=JSONParser.makeHttpRequest("http://198.211.112.25:1234/ranking",parms);
                return output;
            }

            @Override
            protected void onPostExecute(String s) {
                String name[]=new String[5];
                String id[]=new String[5];
                String point[]=new String[5];
                int longitud=0;
                if(s!=null){
                    try {
                        JSONObject jsonObject=new JSONObject(s);
                        JSONArray array=jsonObject.getJSONArray("data");
                        longitud=array.length();
                        longitud=(longitud>5)?5:longitud;
                        for(int i=0;i<longitud;i++){
                            id[i]=array.getJSONObject(i).getString("fbId");
                            name[i]=array.getJSONObject(i).getString("name");
                            point[i]=array.getJSONObject(i).getString("score");
                        }
                        for(int i=0;i<longitud;i++){
                            names[i].setText(name[i]);
                            points[i].setText(point[i]);
                            Picasso.with(getActivity())
                                    .load(StaticData.getLink(id[i]))
                                    .into(photos[i]);
                        }
                        for(int i=longitud;i<5;i++){
                            names[i].setText("-");
                            points[i].setText("-");
                        }
                    } catch (Exception e) {
                        Toast.makeText(getActivity(),"Fallo en la conexion",Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
            }
        }.execute(Profile.getCurrentProfile());
    }
    @Override
    protected void onBackPressed() {
        if(game) {
            for(int i=0;i<2;i++) {
                 getactivity().removeFragment();
            }
            getactivity().getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.back_enter,R.anim.back_exit)
                    .replace(R.id.mainContainer,getactivity().getCurrentFragment())
                    .commit();
        }
        else {
            for(int i=0;i<2;i++) {
                 getactivity().removeFragment();
            }
            getactivity().nextLevelFragment.continuar();
        }
    }
}