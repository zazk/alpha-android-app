package com.example.desarrollador.memoriaalpha;

/**
 * Created by kevin on 05/09/16.
 */
public class StaticData {
    protected static int stickers[]={R.drawable.sticker1,R.drawable.sticker2,R.drawable.sticker3,
            R.drawable.sticker1,R.drawable.sticker2,R.drawable.sticker3,
            R.drawable.sticker1,R.drawable.sticker2,R.drawable.sticker3,
            R.drawable.sticker1,R.drawable.sticker2,R.drawable.sticker3,
            R.drawable.sticker1,R.drawable.sticker2,R.drawable.sticker3};

    protected static boolean hasProfile=false;

    protected static String stickers_ganados="";

    protected static String NAME;

    protected static int LEVEl=0;

    protected static int SUB_LEVEL=0;

    protected static int POINTS= 0;

    protected static int CURRENTPOINTS= 0;

    protected static int HELP=5;

    protected static int PLACE=-1;

    protected static String getLink(String id){
        return "https://graph.facebook.com/"+
                id+"" +
                "/picture?height=100&width=100&migration_overrides=%7Boctober_2012%3Atrue%7D";
    }
    protected static int[] getStickers(){
        if(stickers_ganados.length()!=0) {
            String[] gg = stickers_ganados.split(",");
            int[] sticker = new int[gg.length];
            for (int i = 0; i < sticker.length; i++) {
                sticker[i] = Integer.parseInt(gg[i]);
            }
            return sticker;
        }
        return null;
    }
}
