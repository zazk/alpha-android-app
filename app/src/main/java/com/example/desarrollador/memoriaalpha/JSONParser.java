package com.example.desarrollador.memoriaalpha;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
public class JSONParser {
    public static String makeHttpRequest(String link,HashMap params) {
        try{
            String stream="";
            String data="";

            Iterator it = params.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry e = (Map.Entry)it.next();
                data += URLEncoder.encode(e.getKey().toString(), "UTF-8") + "=" +
                        URLEncoder.encode(e.getValue().toString(), "UTF-8")+"&";
            }
            URL url = new URL(link);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr =  new OutputStreamWriter(conn.getOutputStream());
            wr.write( data );
            wr.flush();
            if(conn.getResponseCode() == 200){
                InputStream in = new BufferedInputStream(conn.getInputStream());
                BufferedReader r = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = r.readLine()) != null) {
                    sb.append(line);
                }
                stream = sb.toString();
                conn.disconnect();
            }
            return stream;
        } catch(Exception e){
            e.printStackTrace();
            return "";
        }
    }
}