package com.example.desarrollador.memoriaalpha;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.squareup.picasso.Picasso;

import java.util.Random;

/**
 * Created by kevin on 14/09/16.
 */
public class LevelFragment extends newFragment {
    protected MyHandler h=new MyHandler();
    protected Runnable r=null;

    protected TextView times=null,points=null;
    protected int option=0;
    protected int time=25;
    protected int correct=0;
    protected int images[]={R.id.carta1,R.id.carta2,R.id.carta3,R.id.carta4,R.id.carta5,R.id.carta6};
    protected ImageView[] paleta=new ImageView[6];
    protected int colors[]=null;
    private View v;
    protected CallbackManager callbackManager;

    boolean first=true;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callbackManager = CallbackManager.Factory.create();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {
        game=true;
        if(v!=null) {
            if(first)
                setRunnable(v);
            return v;
        }
        final View view = inflater.inflate(R.layout.level_game, container, false);
        times=(TextView)view.findViewById(R.id.times);
        points=(TextView)view.findViewById(R.id.puntos);
        Typeface tf = Typeface.createFromAsset(getactivity().getApplicationContext().getAssets(),"fonts/ChineseRocksRg-Regular.ttf");
        points.setTypeface(tf);
        times.setTypeface(tf);
        ((TextView)view.findViewById(R.id.name)).setTypeface(tf);
        ((TextView)view.findViewById(R.id.time_text)).setTypeface(tf);
        ((TextView)view.findViewById(R.id.point_text)).setTypeface(tf);
        ((TextView)view.findViewById(R.id.pause)).setTypeface(tf);
        ((TextView)view.findViewById(R.id.name)).setText(StaticData.NAME.split(" ")[0]);
        Picasso.with(getactivity())
                .load(Profile.getCurrentProfile().getProfilePictureUri(100, 100).toString())
                .into((ImageView) view.findViewById(R.id.profile));
        for(int i=0;i<6;i++) {
            paleta[i]=(ImageView)view.findViewById(images[i]);
            paleta[i].setOnClickListener(null);
        }
        view.findViewById(R.id.pausar).setOnClickListener(null);
        view.findViewById(R.id.profile).setOnClickListener(null);
        view.findViewById(R.id.salir).setOnClickListener(null);
        view.findViewById(R.id.ayuda).setOnClickListener(null);
        view.findViewById(R.id.pausar).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (free&&event.getAction()==MotionEvent.ACTION_DOWN) {
                    free=false;
                    actual=v.getId();
                    return true;
                }
                else if(time>0&&event.getAction()==MotionEvent.ACTION_UP&&actual==v.getId()){
                    actual=-1;
                    onBackPressed();
                    unlock();
                    return true;
                }
                return true;
            }
        });
        view.findViewById(R.id.profile).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (free&&event.getAction()==MotionEvent.ACTION_DOWN) {
                    free = false;
                    actual=v.getId();
                }
                else if(time>0&&event.getAction()==MotionEvent.ACTION_UP&&actual==v.getId()) {
                    actual=-1;
                    openProfile();
                    unlock();
                }
                return false;
            }
        });
        view.findViewById(R.id.salir).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (free&&event.getAction()==MotionEvent.ACTION_DOWN) {
                    free = false;
                    actual=v.getId();
                }
                else if(time>0&&event.getAction()==MotionEvent.ACTION_UP&&actual==v.getId()) {
                    actual=-1;
                    salir();
                    unlock();
                }
                return false;
            }
        });
        setRunnable(view);
        v=view;
        return view;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (callbackManager.onActivityResult(requestCode, resultCode, data)) {
            return;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
    protected void setRunnable(View view) {
    }

    protected void run(){
        h.postDelayed(r,500);
    }
    @Override
    protected void onBackPressed() {
        h.removeCallbacks(r);
        getactivity().putFragment(new PauseFragment());
        getactivity().getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.zoom_in,R.anim.zoom_out)
                .add(R.id.mainContainer,getactivity().getCurrentFragment())
                .commit();
    }
    protected void salir(){
        super.salir();
        h.removeCallbacks(r);
    }
    protected static int[] getRandomPermutation (int length){
        Random r=new Random();
        int[] array = new int[length];
        for(int i = 0; i < array.length; i++)
            array[i] = i;

        for(int i = 0; i < length; i++){

            int ran = i + r.nextInt (length-i);

            int temp = array[i];
            array[i] = array[ran];
            array[ran] = temp;
        }
        return array;
    }
    protected void openProfile(){
        newFragment fg=null;
        if(StaticData.hasProfile) {
            fg=new ProfileFragment();
        }
        else {
            fg = new RegFragment();
        }
        getactivity().putFragment(fg);
        getactivity().getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.enter,R.anim.exit)
                .replace(R.id.mainContainer, getactivity().getCurrentFragment()).commit();
    }
    protected void lose(){
        h.removeCallbacks(r);
        getactivity().putFragment(new LoseFragment());
        getactivity().getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.zoom_in,R.anim.zoom_out)
                .add(R.id.mainContainer,getactivity().getCurrentFragment()).commit();
    }
    @Override
    public void onResume() {
        super.onResume();
        if(getactivity().getCurrentFragment()==this)
            h.postDelayed(r,250);
    }
    @Override
    public void onPause() {
        super.onPause();
        h.removeCallbacks(r);
    }
    protected void ayuda(){
        h.removeCallbacks(r);
        if(StaticData.HELP>0) {
            first=true;
            time=(time>15)?25:time+10;
            option=0;
            StaticData.HELP--;
            h.postDelayed(r,0);
            Toast.makeText(getactivity(),"Ayudas restante: "+StaticData.HELP,Toast.LENGTH_SHORT).show();
            unlock();
        }
        else{
            ShareDialog shareDialog = new ShareDialog(this);
            shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                @Override
                public void onSuccess(Sharer.Result result) {
                    first=true;
                    StaticData.HELP=5;
                    correct=0;
                    option = 0;
                    time=(time>15)?25:time+10;
                    h.postDelayed(r,0);
                    Toast.makeText(getactivity(),"5 Ayudas para este nivel",Toast.LENGTH_SHORT).show();
                    unlock();
                }
                @Override
                public void onCancel() {
                    Toast.makeText(getactivity(),"Se continua el juego",Toast.LENGTH_SHORT).show();
                    h.postDelayed(r, 500);
                    unlock();
                }

                @Override
                public void onError(FacebookException error) {
                    unlock();
                }
            });
            boolean facebookAppFound =   appInstalledOrNot("com.facebook.katana");
            if(facebookAppFound) {
                Bitmap image= BitmapFactory.decodeResource(getContext().getResources(), R.drawable.sticker1);
                SharePhoto photo = new SharePhoto.Builder()
                        .setBitmap(image)
                        .build();
                SharePhotoContent content = new SharePhotoContent.Builder()
                        .addPhoto(photo)
                        .setShareHashtag(new ShareHashtag.Builder()
                                .setHashtag("#MemoriaAlpha")
                                .build())
                        .build();
                shareDialog.show(content);
            }
            else {
                ShareLinkContent content = new ShareLinkContent.Builder()
                        .setContentUrl(Uri.parse("http://forceclose.pe/"))
                        .setShareHashtag(new ShareHashtag.Builder()
                                .setHashtag("#MemoriaAlpha")
                                .build())
                        .setQuote("He conseguido ciertos puntos")
                        .build();
                shareDialog.show(content);

            }

        }
    }

    public void clean() {
        for(int i=0;i<6;i++) {
            paleta[i].setAlpha(1.0f);
            paleta[i].setOnTouchListener(null);
        }
    }
}
