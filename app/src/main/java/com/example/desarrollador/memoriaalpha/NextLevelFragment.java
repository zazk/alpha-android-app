package com.example.desarrollador.memoriaalpha;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NextLevelFragment extends newFragment {
    ShareDialog shareDialog;
    protected CallbackManager callbackManager;
    int ids[]={R.id.sticker1,R.id.sticker2,R.id.sticker3,R.id.sticker4,R.id.sticker5};
    int exc[]={R.drawable.excelente1,R.drawable.excelente2,R.drawable.excelente3};
    ImageView excelente;
    ImageView st[]=new ImageView[5];
    TextView puntos,puesto;
    View view;
    static int im[];
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        hasopened=false;
        game=false;
        im= LevelFragment.getRandomPermutation(15);
        getactivity().putFragment(this);
        if(view!=null) {
            excelente.setImageResource(exc[StaticData.LEVEl]);
            for (int i=0;i<5;i++){
                st[i].setImageResource(StaticData.stickers[im[i]]);
            }
            puesto.setText("Puesto en Ranking: "+StaticData.PLACE+"º");
            puntos.setText("Puntaje: "+StaticData.POINTS);
            return view;
        }
        callbackManager = CallbackManager.Factory.create();
        Typeface tf = Typeface.createFromAsset(getActivity().getApplicationContext().getAssets(),"fonts/ChineseRocksRg-Regular.ttf");
        View view = inflater.inflate(R.layout.cambio_nivel, container, false);
        puntos=(TextView)view.findViewById(R.id.puntos);
        puesto=(TextView)view.findViewById(R.id.puesto);
        excelente=(ImageView)view.findViewById(R.id.excelente);
        excelente.setImageResource(exc[StaticData.LEVEl]);
        for (int i=0;i<5;i++){
            st[i]=((ImageView)view.findViewById(ids[i]));
            st[i].setImageResource(StaticData.stickers[im[i]]);
        }
        puntos.setTypeface(tf);
        puesto.setTypeface(tf);
        puesto.setText("Puesto en Ranking: "+StaticData.PLACE+"º");
        puntos.setText("Puntaje: "+StaticData.POINTS);
        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                new AsyncTask<Profile, Void, String>() {
                    @Override
                    protected String doInBackground(Profile... params) {
                        HashMap<String, String> parms = new HashMap();
                        parms.put("fbId",params[0].getId());
                        parms.put("score",""+100);
                        parms.put("type","1");
                        String output=JSONParser.makeHttpRequest("http://198.211.112.25:1234/addPoints",parms);
                        return output;
                    }
                    @Override
                    protected void onPostExecute(String s) {
                        try {
                            JSONObject jsonObject=new JSONObject(s);
                            StaticData.PLACE=jsonObject.getInt("rank");
                            StaticData.POINTS+=100;
                        } catch (JSONException e) {
                            Toast.makeText(getActivity(),"Conectate a Internet",Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                        unlock();
                    }
                }.execute(Profile.getCurrentProfile());
                Toast.makeText(getActivity(),"5 Ayudas para este nivel",Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onCancel() {
                unlock();
            }
            @Override
            public void onError(FacebookException error) {
                unlock();
            }
        });
        view.findViewById(R.id.continuar).setOnClickListener(null);
        view.findViewById(R.id.versticker).setOnClickListener(null);
        view.findViewById(R.id.comparte).setOnClickListener(null);

        view.findViewById(R.id.continuar).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                    free = false;
                    actual = v.getId();
                } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                    actual=-1;
                    continuar();
                }
                return false;
            }
        });
        view.findViewById(R.id.versticker).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                    free = false;
                    actual = v.getId();
                } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                    actual=-1;
                    StickerFragment fg=new StickerFragment();
                    int m[]=new int[5];
                    for(int i=0;i<5;i++){
                        m[i]=im[i];
                    }
                    Bundle bundle=new Bundle();
                    bundle.putIntArray("stickers",m);
                    fg.setArguments(bundle);
                    getactivity().putFragment(fg);
                    getactivity().getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.enter,R.anim.exit)
                            .replace(R.id.mainContainer,getactivity().getCurrentFragment())
                            .commit();
                    unlock();
                }
                return false;
            }
        });
        view.findViewById(R.id.comparte).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                    free = false;
                    actual = v.getId();
                } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                    actual=-1;
                    boolean facebookAppFound =   appInstalledOrNot("com.facebook.katana");
                    if(facebookAppFound) {
                        List<SharePhoto> list=new ArrayList();
                        for(int i=0;i<5;i++) {
                            Bitmap image = BitmapFactory.decodeResource(getContext().getResources(),
                                    StaticData.stickers[im[i]]);
                            SharePhoto photo = new SharePhoto.Builder()
                                    .setBitmap(image)
                                    .build();
                            list.add(photo);
                        }
                        SharePhotoContent content = new SharePhotoContent.Builder()
                                .addPhotos(list)
                                .setShareHashtag(new ShareHashtag.Builder()
                                        .setHashtag("#MemoriaAlpha")
                                        .build())
                                .build();
                        shareDialog.show(content);
                    }
                    else {
                        ShareLinkContent content = new ShareLinkContent.Builder()
                                .setContentUrl(Uri.parse("http://forceclose.pe/"))
                                .setShareHashtag(new ShareHashtag.Builder()
                                        .setHashtag("#MemoriaAlpha")
                                        .build())
                                .setQuote("He conseguido 100 puntos")
                                .build();
                        shareDialog.show(content);
                    }
                }
                return false;
            }
        });

        this.view=view;
        return view;
    }
    void charge(){
        Log.i("mensaje","mas "+im);
        int aux[]=StaticData.getStickers();
        if(aux!=null) {
            for (int i = 0; i < 5; i++) {
                if(find(aux,im[i])==-1)
                    StaticData.stickers_ganados += "," + im[i];
            }
        }
        else {
            StaticData.stickers_ganados=""+im[0];
            for (int i = 1; i < 5; i++) {
                StaticData.stickers_ganados += "," + im[i];
            }
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (callbackManager.onActivityResult(requestCode, resultCode, data)) {
            return;
        }
    }
    @Override
    protected void onBackPressed() {
    }
    public int find(int array[],int key){
        for(int i=0;i<array.length;i++){
            if(array[i]==key)
                return i;
        }
        return -1;
    }
    void continuar(){
        new AsyncTask<Profile, Void, String>() {
            @Override
            protected String doInBackground(Profile... params) {
                Log.i("mensaje","cargare stickers "+im);
                charge();
                HashMap<String, String> parms = new HashMap();
                parms.put("fbId",params[0].getId());
                parms.put("score",""+StaticData.CURRENTPOINTS);
                parms.put("type","2");
                parms.put("stickers",StaticData.stickers_ganados);
                parms.put("level",((StaticData.LEVEl+1)%3)+"");
                parms.put("phase","0");
                String output=JSONParser.makeHttpRequest("http://198.211.112.25:1234/addPoints",parms);
                return output;
            }
            @Override
            protected void onPostExecute(String s) {
                try {
                    JSONObject jsonObject=new JSONObject(s);
                    StaticData.PLACE=jsonObject.getInt("rank");
                    StaticData.POINTS+=StaticData.CURRENTPOINTS;
                    StaticData.CURRENTPOINTS=0;
                    StaticData.LEVEl=(StaticData.LEVEl+1)%3;
                    StaticData.HELP=5;
                    StaticData.SUB_LEVEL=0;
                    getactivity().removeFragment();
                    getactivity().removeFragment();
                    getactivity().getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.enter,R.anim.exit)
                            .replace(R.id.mainContainer,new StartLevel()).commit();
                    game=true;
                } catch (JSONException e) {
                    Toast.makeText(getActivity(),"Conectate a Internet",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                unlock();
            }
        }.execute(Profile.getCurrentProfile());
    }
}