package com.example.desarrollador.memoriaalpha;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v4.app.Fragment;

/**
 * Created by kevin on 12/09/16.
 */
public class newFragment extends Fragment {
    static boolean game=true;
    static boolean hasopened=false;
    boolean free=true;
    int actual=-1;
    protected void onBackPressed(){}
    protected boolean appInstalledOrNot(String uri) {
        PackageManager pm = getactivity().getPackageManager();
        boolean app_installed = false;
        try {
            ApplicationInfo ai=pm.getApplicationInfo(uri,0);

            app_installed = ai.enabled;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }
    protected void salir() {
        getactivity().putFragment(new CloseFragment());
        getactivity().getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.zoom_in,R.anim.zoom_out)
                .add(R.id.mainContainer,getactivity().getCurrentFragment()).commit();
    }
    protected void unlock(){
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                free=true;
            }
        },125);
    }
    protected MainActivity getactivity(){

        return MainActivity.getInstance();
       // return (MainActivity)getActivity();
    }

}
