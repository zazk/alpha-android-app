package com.example.desarrollador.memoriaalpha;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.Profile;
import com.squareup.picasso.Picasso;

/**
 * Created by kevin on 14/09/16.
 */
public class TermFragment extends newFragment {
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        if(view!=null)
            return view;
        View v = inflater.inflate(R.layout.term_fragment, container, false);
        Typeface tf = Typeface.createFromAsset(getActivity().getApplicationContext().getAssets(),"fonts/ChineseRocksRg-Regular.ttf");
        ((TextView)v.findViewById(R.id.texto1)).setTypeface(tf);
        ((TextView)v.findViewById(R.id.texto2)).setTypeface(tf);
        ((TextView)v.findViewById(R.id.texto3)).setTypeface(tf);
        ((TextView)v.findViewById(R.id.texto4)).setTypeface(tf);
        ((TextView)v.findViewById(R.id.text)).setTypeface(tf);
        v.findViewById(R.id.salir).setOnClickListener(null);
        v.findViewById(R.id.continuar).setOnClickListener(null);
        v.findViewById(R.id.profile).setOnClickListener(null);
        v.findViewById(R.id.salir).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                    free = false;
                    actual = v.getId();
                } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                    actual=-1;
                    salir();
                    unlock();
                }
                return false;
            }
        });
        v.findViewById(R.id.continuar).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                    free = false;
                    actual = v.getId();
                } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                    actual=-1;
                    onBackPressed();
                    unlock();
                }
                return false;
            }
        });
        Picasso.with(getactivity())
                .load(Profile.getCurrentProfile().getProfilePictureUri(100, 100).toString())
                .into((ImageView)v.findViewById(R.id.profile));
        view=v;
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(!game&&!hasopened){
            view.findViewById(R.id.profile).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                        free = false;
                        actual = v.getId();
                    } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                        actual=-1;
                        hasopened=true;
                        newFragment fg=null;
                        if(StaticData.hasProfile) {
                            fg=new ProfileFragment();
                        }
                        else {
                            fg = new RegFragment();
                        }
                        getactivity().removeFragment();
                        getactivity().putFragment(fg);
                        getactivity().getSupportFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(R.anim.up_enter,R.anim.up_exit)
                                .replace(R.id.mainContainer,getactivity().getCurrentFragment())
                                .addToBackStack(null)
                                .commit();
                        unlock();
                    }
                    return false;
                }
            });
        }
        else
        view.findViewById(R.id.profile).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                    free = false;
                    actual = v.getId();
                } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                    actual=-1;
                    for(int i=0;i<3;i++) {
                        getactivity().removeFragment();
                    }
                    getactivity().getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.back_enter,R.anim.back_exit)
                            .replace(R.id.mainContainer,getactivity().getCurrentFragment())
                            .commit();
                    unlock();
                }
                return false;
            }
        });
    }
    @Override
    protected void onBackPressed() {
        if(game) {
            for(int i=0;i<4;i++) {
                getactivity().removeFragment();
            }
            getactivity().getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.back_enter,R.anim.back_exit)
                    .replace(R.id.mainContainer,getactivity().getCurrentFragment())
                    .commit();
        }
        else {
            if(hasopened)
                for(int i=0;i<3;i++){
                    getactivity().removeFragment();
                }
            getactivity().removeFragment();
            getactivity().nextLevelFragment.continuar();
        }
    }
}
