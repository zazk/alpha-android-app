package com.example.desarrollador.memoriaalpha;

import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by kevin on 13/09/16.
 */
public class ImageFragment extends Fragment {
    ViewPager viewPager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.image_fragment, container, false);
        ((ImageView)v.findViewById(R.id.sticker)).setImageResource(StaticData.stickers[getArguments().getInt("dia")]);
        viewPager=(ViewPager)container;
        v.findViewById(R.id.left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("mensaje",""+viewPager.getCurrentItem()+" "+viewPager.getChildCount());
                if(viewPager.getCurrentItem()>0)
                    viewPager.setCurrentItem(viewPager.getCurrentItem()-1);
            }
        });
        v.findViewById(R.id.right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("mensaje",""+viewPager.getCurrentItem()+" "+viewPager.getAdapter().getCount());
                if(viewPager.getCurrentItem()<viewPager.getAdapter().getCount()-1)
                    viewPager.setCurrentItem(viewPager.getCurrentItem()+1);
            }
        });
        return v;
    }
}
