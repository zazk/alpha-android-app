package com.example.desarrollador.memoriaalpha;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Profile;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class RegFragment extends newFragment {
    int sti=LevelFragment.getRandomPermutation(15)[0];
    EditText nombre,dni,correo,telefono;
    TextView dias,mess,anios;
    RadioButton male,female;
    String fecha=null,genero="1";
    int dia,mes,anio;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view=inflater.inflate(R.layout.registro_fragment, container, false);
        Typeface tf = Typeface.createFromAsset(getActivity().getApplicationContext().getAssets(),"fonts/ChineseRocksRg-Regular.ttf");
        male=((RadioButton)view.findViewById(R.id.male));
        female=((RadioButton)view.findViewById(R.id.female));
        CompoundButton.OnCheckedChangeListener listener=new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                boolean checked = buttonView.isChecked();
                // Check which radio button was clicked
                switch(buttonView.getId()) {
                    case R.id.male:
                        if (checked) {
                            genero = "1";
                            break;
                        }
                    case R.id.female:
                        if (checked) {
                            genero = "0";
                            break;
                        }
                }
            }
        };
        male.setOnCheckedChangeListener(listener);
        female.setOnCheckedChangeListener(listener);

        view.findViewById(R.id.fecha).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(fecha==null){
                    /*
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    String currentDateandTime = sdf.format(new Date());
                    dia=Integer.parseInt(currentDateandTime.substring(0,2));
                    mes=Integer.parseInt(currentDateandTime.substring(3,5));
                    anio=Integer.parseInt(currentDateandTime.substring(6,10));
                    Log.i("mensaje",dia+" "+mes+" "+anio);*/
                    dia=1;
                    mes=0;
                    anio=1990;
                }
                new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                dia=dayOfMonth;
                                mes=monthOfYear;
                                anio=year;
                                fecha=dia+"/"+(mes+1)+"/"+anio;
                                mess.setText((mes+1)/10+""+(mes+1)%10);
                                dias.setText(dia/10+""+dia%10);
                                anios.setText(""+anio);
                            }
                        },
                        anio,
                        mes,
                        dia).show();
            }
        });
        nombre=((EditText)view.findViewById(R.id.nombre));
        correo=((EditText)view.findViewById(R.id.correo));
        dni=((EditText)view.findViewById(R.id.dni));
        telefono=((EditText)view.findViewById(R.id.telefono));
        dias=(TextView)view.findViewById(R.id.dia);
        mess=(TextView)view.findViewById(R.id.mes);
        anios=(TextView)view.findViewById(R.id.anio);

        dias.setTypeface(tf);
        mess.setTypeface(tf);
        anios.setTypeface(tf);
        telefono.setTypeface(tf);
        nombre.setTypeface(tf);
        correo.setTypeface(tf);
        dni.setTypeface(tf);
        male.setTypeface(tf);
        female.setTypeface(tf);

        view.findViewById(R.id.completar).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (free&&event.getAction()==MotionEvent.ACTION_DOWN) {
                    free = false;
                    actual=v.getId();
                }
                else if(event.getAction()==MotionEvent.ACTION_UP&&actual==v.getId()) {
                    actual=-1;
                    if(validar()) {
                        Bundle bundle=new Bundle();
                        bundle.putString("fbId", Profile.getCurrentProfile().getId());
                        bundle.putString("name", nombre.getText().toString());
                        bundle.putString("dni",dni.getText().toString());
                        bundle.putString("phone",telefono.getText().toString());
                        bundle.putString("email", correo.getText().toString());
                        bundle.putString("dateOfBirth", fecha);
                        bundle.putString("gender", genero);
                        bundle.putString("stickers", "" + sti);
                        Fragment conf=new DatosDIalog();
                        conf.setArguments(bundle);
                        getactivity().getSupportFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(R.anim.zoom_in,R.anim.zoom_out)
                                .add(R.id.mainContainer,conf)
                                .commit();
                    }
                    unlock();
                }
                return false;
            }
        });
        view.findViewById(R.id.completar).setOnClickListener(null);
        return view;
    }
    @Override
    protected void onBackPressed() {
    }
    protected boolean validar(){
        if(!nombre.getText().toString().matches("[a-zA-Z ]+")) {
            Toast.makeText(getActivity(),"Revisa tu nombe",Toast.LENGTH_SHORT).show();
            return false;
        }
        if(!dni.getText().toString().matches("[0-9]{8}")){
            Toast.makeText(getActivity(),"Revisa tu DNI",Toast.LENGTH_SHORT).show();
            return false;
        }
        if(!telefono.getText().toString().matches("[0-9]{6,9}")){
            Toast.makeText(getActivity(),"Revisa tu Telefono",Toast.LENGTH_SHORT).show();
            return false;
        }
        if(!correo.getText().toString().matches("[a-zA-Z_.0-9]+@[a-zA-Z.]+")){
            Toast.makeText(getActivity(),"Revisa tu correo",Toast.LENGTH_SHORT).show();
            return false;
        }
        if(fecha==null){
            Toast.makeText(getActivity(),"Ingrese una fecha",Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
