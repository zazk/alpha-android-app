package com.example.desarrollador.memoriaalpha;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.facebook.Profile;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by kevin on 31/08/16.
 */
public class NextSubLevelFragment extends newFragment {
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((MainActivity)getActivity()).putFragment(this);
        if(view!=null) {
            return view;
        }
        View v = inflater.inflate(R.layout.cambio_subnivel, container, false);
        v.findViewById(R.id.continuar).setOnClickListener(null);
        v.findViewById(R.id.continuar).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                    free = false;
                    actual = v.getId();
                } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                    actual=-1;
                    new AsyncTask<Profile, Void, String>() {
                        @Override
                        protected String doInBackground(Profile... params) {
                            HashMap<String, String> parms = new HashMap();
                            parms.put("fbId",params[0].getId());
                            parms.put("score",""+StaticData.CURRENTPOINTS);
                            parms.put("type","0");
                            parms.put("level",StaticData.LEVEl+"");
                            parms.put("phase",(StaticData.SUB_LEVEL+1)+"");
                            String output=JSONParser.makeHttpRequest("http://198.211.112.25:1234/addPoints",parms);
                            return output;
                        }
                        @Override
                        protected void onPostExecute(String s) {
                            try {
                                JSONObject jsonObject=new JSONObject(s);
                                StaticData.PLACE=jsonObject.getInt("rank");
                                StaticData.POINTS+=StaticData.CURRENTPOINTS;
                                StaticData.CURRENTPOINTS=0;
                                StaticData.SUB_LEVEL++;
                                LevelFragment fg=null;
                                switch (StaticData.LEVEl){
                                    case 0:
                                        fg=new FirstLevelFragment();
                                        break;
                                    case 1:
                                        fg=new SecondLevelFragment();
                                        break;
                                    case 2:
                                        fg=new ThirdLevelFragment();
                                        break;
                                }
                                getactivity().removeFragment();
                                getactivity().removeFragment();
                                getactivity().putFragment(fg);
                                getactivity().getSupportFragmentManager().beginTransaction()
                                        .setCustomAnimations(R.anim.enter, R.anim.exit)
                                        .replace(R.id.mainContainer, fg).commit();
                            } catch (JSONException e) {
                                Toast.makeText(getActivity(),"Conectate a Internet",Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                            unlock();
                        }
                    }.execute(Profile.getCurrentProfile());
                }
                return false;
            }
        });
        view=v;
        return v;
    }
    @Override
    protected void onBackPressed() {
    }
}
