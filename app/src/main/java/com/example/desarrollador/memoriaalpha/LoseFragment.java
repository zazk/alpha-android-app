package com.example.desarrollador.memoriaalpha;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.Profile;
import com.squareup.picasso.Picasso;

/**
 * Created by kevin on 20/09/16.
 */
public class LoseFragment extends newFragment {
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lose_fragment, container, false);
        view.findViewById(R.id.volver).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LevelFragment fg=getactivity().getLevelFragment();
                StaticData.CURRENTPOINTS=0;
                fg.time=25;
                fg.option=0;
                fg.first=true;
                fg.correct=0;
                fg.clean();
                getactivity().removeFragment();
                getactivity().getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.zoom_in,R.anim.zoom_out)
                        .remove(LoseFragment.this).commit();
                fg.h.postDelayed(fg.r,250);
            }
        });
        view.findViewById(R.id.salir).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salir();
            }
        });
        this.view=view;
        return view;
    }
    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        final int animatorId = (enter) ? R.anim.zoom_in : R.anim.zoom_out;
        final Animation anim = AnimationUtils.loadAnimation(getActivity(), animatorId);
        anim.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setBackgroundColor(Color.parseColor("#96c6e431"));
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        view.setBackgroundColor(Color.TRANSPARENT);
        return anim;
    }
}
