package com.example.desarrollador.memoriaalpha;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.facebook.FacebookSdk;
import com.facebook.login.*;
import com.facebook.login.LoginFragment;

import java.util.Vector;


public class MainActivity extends AppCompatActivity {
    Vector<newFragment> pila=new Vector();
    RankingFragment rankingFragment;
    PremioFragment premioFragment;
    TermFragment termFragment;
    NextSubLevelFragment nextSubLevelFragment;
    NextLevelFragment nextLevelFragment;
    private static MainActivity instance;
    public static MainActivity getInstance() {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        instance=this;
        super.onCreate(savedInstanceState);
        if(savedInstanceState==null) {
            FacebookSdk.sdkInitialize(getApplicationContext());
            setContentView(R.layout.activity_login);
            //getSupportFragmentManager().beginTransaction().replace(R.id.mainContainer,new LoginFragment()).commit();
        }
    }
    public void nada(View view){

    }
    public void putFragment(newFragment fg){
        pila.add(fg);
        Log.i("mensajenuevo","agregado tamanio: "+pila.size()+" "+pila.get(pila.size() - 1).getClass().getName());
    }
    public LevelFragment getLevelFragment(){
        if(pila.get(0) instanceof LevelFragment)
            return (LevelFragment)pila.get(0);
        return null;
    }
    public newFragment getCurrentFragment(){
        //Log.i("mensajenuevo","current tamanio: "+pila.size());
        if(pila.size()>0) {
            Log.i("mensajenuevo","current fragment: "+pila.size()+" "+pila.get(pila.size() - 1).getClass().getName());
            return pila.get(pila.size() - 1);
        }
        return null;
    }
    public void removeFragment(){
        //Log.i("mensajenuevo","eliminado tamanio: "+pila.size());
        if(pila.size()>0) {
            Log.i("mensajenuevo","eliminado tamanio: "+pila.size()+" "+pila.get(pila.size() - 1).getClass().getName());

            pila.remove(pila.size() - 1);
        }
    }
    @Override
    public void onBackPressed() {
        if(pila.size()>0)
            getCurrentFragment().onBackPressed();
        else super.onBackPressed();
    }
}
