package com.example.desarrollador.memoriaalpha;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.util.Stack;

/**
 * Created by kevin on 05/09/16.
 */
class MyHandler extends Handler {
    Stack<Message> s = new Stack<Message>();
    boolean is_paused = false;

    public synchronized void pause() {
        is_paused = true;
    }

    public synchronized void resume() {
        is_paused = false;
        while (!s.empty()) {
            sendMessageAtFrontOfQueue(s.pop());
        }
    }


    @Override
    public void handleMessage(Message msg) {
        Log.i("mensaje","estoy en el handler");
        if (is_paused) {
            s.push(Message.obtain(msg));
            return;
        }
        else{
            super.handleMessage(msg);
        }
    }
    //...
}