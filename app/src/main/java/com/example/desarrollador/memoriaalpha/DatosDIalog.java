package com.example.desarrollador.memoriaalpha;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.facebook.Profile;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by kevin on 21/09/16.
 */
public class DatosDIalog extends newFragment{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view=inflater.inflate(R.layout.datosdialog, container, false);
        ((MainActivity)getActivity()).putFragment(this);
        view.findViewById(R.id.continuar).setOnClickListener(null);
        view.findViewById(R.id.corregir).setOnClickListener(null);
        view.findViewById(R.id.continuar).setOnTouchListener(
                new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                            free = false;
                            actual = v.getId();
                        } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                            actual=-1;
                            Bundle bundle=getArguments();
                            new AsyncTask<String, Void, String>() {
                                String nombre;
                                String sti;

                                @Override
                                protected String doInBackground(String... params) {
                                    HashMap<String, String> parms = new HashMap();
                                    nombre = params[0];
                                    sti=params[6];
                                    parms.put("fbId", Profile.getCurrentProfile().getId());
                                    parms.put("name", params[0]);
                                    parms.put("dni", params[1]);
                                    parms.put("phone", params[2]);
                                    parms.put("email", params[3]);
                                    parms.put("dateOfBirth", params[4]);
                                    parms.put("gender", params[5]);
                                    parms.put("stickers",params[6]);
                                    String output = JSONParser.makeHttpRequest("http://198.211.112.25:1234/complete", parms);
                                    return output;
                                }

                                @Override
                                protected void onPostExecute(String s) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(s);
                                        if (jsonObject.getInt("response") == 1) {
                                            StaticData.hasProfile = true;
                                            StaticData.NAME = nombre;
                                            StaticData.POINTS = 0;
                                            StaticData.PLACE = 0;
                                            StaticData.stickers_ganados = sti;
                                        }
                                        getactivity().removeFragment();
                                        getactivity().removeFragment();
                                        getactivity().putFragment(new ProfileFragment());

                                        getactivity().getSupportFragmentManager()
                                                .beginTransaction()
                                                .setCustomAnimations(R.anim.enter,R.anim.exit)
                                                .replace(R.id.mainContainer,getactivity().getCurrentFragment())
                                                .commit();
                                    } catch (Exception e) {
                                        Toast.makeText(getActivity(), "Conectate a Internet", Toast.LENGTH_SHORT).show();
                                        e.printStackTrace();
                                    }
                                }
                            }.execute(
                                    bundle.getString("name"),
                                    bundle.getString("dni"),
                                    bundle.getString("phone"),
                                    bundle.getString("email"),
                                    bundle.getString("dateOfBirth"),
                                    bundle.getString("gender"),
                                    bundle.getString("stickers"));
                            unlock();
                        }
                        return false;
                    }
                });
        view.findViewById(R.id.corregir).setOnTouchListener(
                new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                            free = false;
                            actual = v.getId();
                        } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                            actual=-1;
                            onBackPressed();
                            unlock();
                        }
                        return false;
                    }
                });
        return view;
    }
    @Override
    protected void onBackPressed() {
        getactivity().removeFragment();
        getactivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }
}
