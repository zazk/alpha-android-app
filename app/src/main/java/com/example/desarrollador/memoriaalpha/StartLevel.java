package com.example.desarrollador.memoriaalpha;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.facebook.login.LoginManager;

public class StartLevel extends newFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getactivity().putFragment(this);
        View view = inflater.inflate(R.layout.start_level, container, false);
        switch (StaticData.LEVEl){
            case 0:
                ((ImageView)view.findViewById(R.id.star)).setImageResource(R.drawable.star1);
                break;
            case 1:
                ((ImageView)view.findViewById(R.id.star)).setImageResource(R.drawable.star2);
                break;
            case 2:
                ((ImageView)view.findViewById(R.id.star)).setImageResource(R.drawable.star3);
                break;
        }
        view.findViewById(R.id.jugar).setOnClickListener(null);
        view.findViewById(R.id.salir).setOnClickListener(null);
        view.findViewById(R.id.jugar).
                setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (free&&event.getAction()==MotionEvent.ACTION_DOWN) {
                            free=false;
                            actual=v.getId();
                        }
                        else if(event.getAction()==MotionEvent.ACTION_UP&&actual==v.getId()){
                            actual=-1;
                            getactivity().removeFragment();
                            StaticData.HELP=5;
                            LevelFragment fg=null;
                            switch (StaticData.LEVEl){
                                case 0:
                                    fg=new FirstLevelFragment();
                                    break;
                                case 1:
                                    fg=new SecondLevelFragment();
                                    break;
                                case 2:
                                    fg=new ThirdLevelFragment();
                                    break;
                            }
                            getactivity().putFragment(fg);
                            getactivity().getSupportFragmentManager().beginTransaction()
                                    .setCustomAnimations(R.anim.enter, R.anim.exit)
                                    .replace(R.id.mainContainer,getactivity().getCurrentFragment()).commit();
                            unlock();
                        }
                        return false;
                    }
                });
        view.findViewById(R.id.salir).
                setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (free && event.getAction() == MotionEvent.ACTION_DOWN) {
                            free = false;
                            actual = v.getId();
                        } else if (event.getAction() == MotionEvent.ACTION_UP && actual == v.getId()) {
                            actual=-1;
                            salir();
                            unlock();
                        }
                        return false;
                    }
                });
        return view;
    }
    @Override
    protected void onBackPressed() {
        salir();
    }
}
